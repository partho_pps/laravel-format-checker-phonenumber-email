<?php

namespace Developer\LaravelBdPhoneEmailFormatChecker;

use Carbon\Laravel\ServiceProvider;

class formatCheckerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('bd-phone-email-checker', function ($app) {
            return new \Developer\LaravelBdPhoneEmailFormatChecker\formatChecker();
        });

        $this->mergeConfigFrom(
            __DIR__.'/../config/formatChecker.php', 'formatChecker'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/formatChecker.php' => config_path('formatChecker.php'),
        ]);
    }
}
