<?php

namespace Developer\LaravelBdPhoneEmailFormatChecker;

class formatChecker
{
    public function phoneNumberFormat($subject = true,$country_code = null)
    {
        if(is_null($country_code)){
            $country_code = config('formatChecker.countryCode');
        }
        if($country_code == 'bn'){
            $pattern = '/^01[3-9]\d{8}$/';
            return preg_match($pattern, $subject);
        }else{
            return 'not found';
        }
        
    }

     public function emailCheck($email)
     {
         if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
             return $data = 1 ;
         } else {
             return $data = 0;
         }
     }
}
